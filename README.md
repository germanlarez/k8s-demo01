This configures a mongoDB alg with a mongo express using kubernetes.

- MongoDB pod.
- Internal service to talk to mongoDB pod.
- Mongo Express deployment to manage MongoDB.
- Credentials or Secret for DB user and password.
- ConfigMap for communication endpoints as the DB url.
- External Service to talk to Mongo Express.

[Demo Architecture](/images/demo.png)

### kubectl apply commands in order
    
    kubectl apply -f mongo-secret.yaml
    kubectl apply -f mongo.yaml
    kubectl apply -f mongo-configmap.yaml 
    kubectl apply -f mongo-express.yaml

### kubectl get commands

    kubectl get pod
    kubectl get pod --watch
    kubectl get pod -o wide
    kubectl get service
    kubectl get secret
    kubectl get all | grep mongodb

### kubectl debugging commands

    kubectl describe pod mongodb-deployment-xxxxxx
    kubectl describe service mongodb-service
    kubectl logs mongo-express-xxxxxx

### give a URL to external service in minikube

    minikube service mongo-express-service